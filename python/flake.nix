{
  description = "Default Python flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    mach-nix.url = "github:DavHau/mach-nix";
  };

  outputs = { self, nixpkgs, flake-utils, mach-nix, ... }: let
    system = "x86_64-linux";
    pkgs = nixpkgs.legacyPackages."${system}";
    customPython = mach-nix.lib."${system}".mkPython {
      requirements = builtins.readFile ./requirements.txt;
    };
  in rec {
    defaultTemplate = {
      path = ./.;
      description = "Nix flake for python projects";
    };

    devShell."${system}" = pkgs.mkShell {
        buildInputs = with pkgs; [
          customPython
        ];

        shellHook = ''
          export QT_QPA_PLATFORM="xcb"
        '';
      };
  };
}
