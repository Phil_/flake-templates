{
  description = "Default Python flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils}: let
    system = "x86_64-linux";
    pkgs = nixpkgs.legacyPackages."${system}";
  in rec {
    defaultTemplate = templates."python";

    templates."python" = {
      path = ./python;
      description = "Python template";
    };

    templates."python-jupyter" = {
      path = ./python-jupyter;
      description = "Python template with jupyter lab (supports extensions)";
    };
  };
}
