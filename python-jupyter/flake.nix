{
  description = "Default Python flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    mach-nix.url = "github:DavHau/mach-nix";
  };

  outputs = { self, nixpkgs, flake-utils, mach-nix, ... }: let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };

    customPython = mach-nix.lib."${system}".mkPython {
      requirements = (builtins.readFile ./requirements.txt) + ''
        jupyterlab
        jupyter_ascending
      '';
    };

    python3 = customPython.python.pkgs;
    jupyter_extension = "${python3.jupyterlab}/bin/jupyter-labextension";
    jupyterDir = "${python3.jupyterlab}/lib/python3.8";
    directory = "./jupyterlab";

  in rec {
    devShell."${system}" = pkgs.mkShell {
        buildInputs = with pkgs; [
          customPython
          nodejs
        ];

        shellHook = ''
          export QT_QPA_PLATFORM="xcb"

          if [ ! -d ${directory} ]; then
            mkdir -p ${directory}/staging
            cp ${jupyterDir}/site-packages/jupyterlab/staging/yarn.lock ${directory}/staging
            chmod +w ${directory}/staging/yarn.lock

            ${jupyter_extension} install jupyterlab-jupytext@1.2.2 --app-dir=${directory} --generate-config 2>/dev/null
            ${jupyter_extension} install jupyterlab-plotly --app-dir=${directory} --generate-config 2>/dev/null

            chmod +rw ${directory} -R
            jupyter lab build --app-dir=${directory}
          fi
        '';
      };
  };
}
